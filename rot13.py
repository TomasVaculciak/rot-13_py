from string import ascii_letters,ascii_lowercase

toDecrypt = input(str("ROT-13 Encrypted Message(no colons,spaces or dots): "))
alphabet = ["a","b", "c", "d", "e", "f", "g" ,"h" ,"i", "j" ,"k" ,"l", "m" ,"n", "o", "p", "q", "r" ,"s" ,"t" ,"u" ,"v" ,"w", "x", "y" ,"z"]
array = []
def split_str(toDecrypt):
    return [c for c in toDecrypt]
array_toDecrypt = split_str(toDecrypt)
arrayTD_numbers = [ascii_letters.index(letter) + 1 for letter in array_toDecrypt]
for number in arrayTD_numbers:
    newNumber = number - 13
    if newNumber<0:
      numberZero_case = newNumber +26
      array.append(numberZero_case)
    else:
     array.append(newNumber)
    
print(array)
def convert_to_letters(array: list) -> str:
  
    letters = [ascii_lowercase[num - 1] for num in array]
  
    return ''.join(letters)
    
decryptedMessage = convert_to_letters(array)
print(decryptedMessage)


