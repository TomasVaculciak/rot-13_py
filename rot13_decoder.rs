fn rot13(input: &str) -> String {
    input
        .chars()
        .map(|c| {
            if c.is_ascii_alphabetic() {
                let offset = if c.is_ascii_uppercase() { b'A' } else { b'a' };
                (((c as u8 - offset + 13) % 26) + offset) as char
            } else {
                c
            }
        })
        .collect()
}

fn rot13_decode(input: &str) -> String {
    rot13(input)
}

fn main() {
    let input = "grfg";
    let output = rot13_decode(input);
    println!("{}", output); 
}